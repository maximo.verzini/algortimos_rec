#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_SIZE 1000

#define data_format "%u -> *%c*\n"

static void print_help(char *exec_name)
{
    fprintf(stdout, "Show format: %s format\n", exec_name);
    fprintf(stdout, "Usage: %s <filepath>\n", exec_name);
    fprintf(stdout, "filepath: path to the file containing the data\n");
}

static void print_data_format()
{
    fprintf(stdout, "Data must follow this format:\n");
    fprintf(stdout, "%%d -> *%%c*\n");
    fprintf(stdout, "where:\n");
    fprintf(stdout, "%%d: unsigned int index, 0 <= index <= %d \n", MAX_SIZE);
    fprintf(stdout, "%%c: char character\n");
    fprintf(stdout, "\n");
    fprintf(stdout, "Example:\n");
    fprintf(stdout, data_format, 3, 'A');
    fprintf(stdout, data_format, 4, 'B');
    fprintf(stdout, data_format, 5, 'C');
    fprintf(stdout, "\n");
}

static char *parse_args(int argc, char **argv)
{
    char *exec_name = argv[0];
    char *filepath = NULL;

    if (argc != 2)
    {
        fprintf(stderr, "Invalid number of arguments\n");
        fprintf(stderr, "Expected 2, given %d\n", argc);
        print_help(exec_name);
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[1], "help") == 0)
    {
        print_help(exec_name);
        exit(EXIT_SUCCESS);
    }

    else if (strcmp(argv[1], "format") == 0)
    {
        print_data_format();
        exit(EXIT_SUCCESS);
    }

    filepath = argv[1];
    return filepath;
}

static void dump(char a[], unsigned int length)
{
    printf("\"");
    for (unsigned int j = 0u; j < length; j++)
    {
        printf("%c", a[j]);
    }
    printf("\"");
    printf("\n\n");
}

unsigned int
data_from_file(const char *path,
               unsigned int indexes[],
               char letters[],
               unsigned int max_size)
{
    unsigned int length = 0u;

    FILE *file = NULL;

    file = fopen(path, "r");

    if (file == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    char read_char = ' ';
    unsigned int read_index = 0;
    int read_amount = 0u;

    while ((read_amount = fscanf(file, data_format, &read_index, &read_char)) &&
           read_amount != EOF &&
           read_amount == 2 &&
           length < max_size)
    {
        if (read_index > max_size)
        {
            fprintf(stderr, "Invalid index, out of bounds %u is not between 0 and %u.\n", read_index, max_size);
            print_data_format();
            exit(EXIT_FAILURE);
        }
        indexes[length] = read_index;
        letters[length] = read_char;
        length++;
    }

    if (length >= max_size && !feof(file))
    {
        fprintf(stderr, "Invalid data format");
        print_data_format();
        exit(EXIT_FAILURE);
    }

    fclose(file);

    return length;
}

int main(int argc, char **argv)
{
    unsigned int indexes[MAX_SIZE];
    char letters[MAX_SIZE];
    char sorted[MAX_SIZE];
    unsigned int length = 0;
    //  .----------^
    //  :
    // Debe guardarse aqui la cantidad de elementos leidos del archivo

    /* -- completar -- */

    char *filepath;

    filepath = parse_args(argc, argv);

    length = data_from_file(filepath, indexes, letters, MAX_SIZE);

    for (size_t i = 0; i < length; i++)
    {
        sorted[indexes[i]] = letters[i];
    }

    dump(sorted, length);

    return EXIT_SUCCESS;
}
