#ifndef WEATHER_UTILS_H
#define WEATHER_UTILS_H

#include "array_helpers.h"

/// @brief Retrieves the lowest registered temperature on a.
/// @param a Table with the data
/// @return the lowest temperature registered in a.
int get_lowest_temperature(WeatherTable a);

/// @brief Registers the highest temperature in each year.
/// @param a Table with the data.
/// @param result Output array that will contain the result.
void register_highest_temperature_by_year(WeatherTable a, int result[YEARS]);

/// @brief Registers the month with the most precipitations in each year.
/// @param a Table with the data.
/// @param result Output array that will contain the result.
void register_rainiest_month_by_year(WeatherTable a, month_t result[YEARS]);

#endif