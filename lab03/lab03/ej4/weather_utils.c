#include "weather_utils.h"

int get_lowest_temperature(WeatherTable a)
{

    int minimum_temperature = a[0][january][0]._min_temp;

    for (int year = 0; year < YEARS; year++)
    {
        for (month_t month = january; month < december; month++)
        {
            for (unsigned int day = 0; day < 28; day++)
            {
                if (a[year][month][day]._min_temp < minimum_temperature)
                    minimum_temperature = a[year][month][day]._min_temp;
            }
        }
    }

    return minimum_temperature;
}

void register_highest_temperature_by_year(WeatherTable a, int result[YEARS])
{
    for (int year = 0; year < YEARS; year++)
    {
        month_t current_hottest_month = january;
        int current_highest_temperature = a[year][january][0]._max_temp;

        for (month_t month = january; month < december; month++)
        {
            for (unsigned int day = 0; day < 28; day++)
            {
                if (current_highest_temperature < a[year][month][day]._max_temp)
                {
                    current_highest_temperature = a[year][month][day]._max_temp;
                    current_hottest_month = month;
                }
            }
        }

        result[year] = current_hottest_month;
    }
}

void register_rainiest_month_by_year(WeatherTable a, month_t result[YEARS])
{
    for (int year = 0; year < YEARS; year++)
    {
        month_t current_rainiest_month = january;
        unsigned int current_highest_precipitation = a[year][january][0]._rainfall;

        for (month_t month = january; month < december; month++)
        {
            unsigned int current_precipitation = 0;
            for (unsigned int day = 0; day < 28; day++)
            {
                current_precipitation += a[year][month][day]._rainfall;
            }
            if (current_highest_precipitation < current_precipitation)
            {
                current_highest_precipitation = current_precipitation;
                current_rainiest_month = month;
            }
        }

        result[year] = current_rainiest_month;
    }
}
