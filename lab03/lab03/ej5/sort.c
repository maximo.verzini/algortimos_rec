/*
  @file sort.c
  @brief sort functions implementation
*/

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "helpers.h"
#include "sort.h"
#include "player.h"

bool goes_before(player_t x, player_t y)
{
    if (x.rank > y.rank)
        return false;
    return true;
}

bool array_is_sorted(player_t atp[], unsigned int length)
{
    unsigned int i = 1u;
    while (i < length && goes_before(atp[i - 1u], atp[i]))
    {
        i++;
    }
    return (i == length);
}

void swap(player_t players[], unsigned int a, unsigned int b)
{
    player_t aux = players[a];
    players[a] = players[b];
    players[b] = aux;
}

unsigned int q_sort_pivot(player_t players[], unsigned int left, unsigned int right)
{
    unsigned int pivot = left;
    unsigned int d_left = left > right ? right : left + 1;
    unsigned int d_right = right < left ? left : right;

    while (d_left <= d_right)
    {
        if (goes_before(players[d_left], players[pivot]))
        {
            d_left++;
        }
        else if (goes_before(players[pivot], players[d_right]))
        {
            d_right--;
        }
        else
        {
            swap(players, d_left, d_right);
            d_left++;
            d_right--;
        }
    }

    swap(players, pivot, d_right);
    pivot = d_right;

    return pivot;
}

void q_sort_rec(player_t players[], unsigned int left, unsigned int right)
{
    if (left >= right)
        return;

    unsigned int pivot = q_sort_pivot(players, left, right);

    if (pivot > left)
        q_sort_rec(players, left, pivot - 1);

    if (pivot < right)
        q_sort_rec(players, pivot + 1, right);
}

void sort(player_t players[], unsigned int length)
{
    q_sort_rec(players, 0, (length == 0) ? 0 : length - 1);
}
