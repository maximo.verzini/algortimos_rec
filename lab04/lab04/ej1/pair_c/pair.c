#include <stdlib.h>
#include "pair.h"

struct s_pair_t
{
    int fst;
    int snd;
};

pair_t pair_new(int x, int y)
{
    pair_t p = malloc(sizeof(struct s_pair_t));
    *p = (struct s_pair_t){x, y};
    return p;
}

int pair_first(pair_t p)
{
    return p->fst;
}

int pair_second(pair_t p)
{
    return p->snd;
}

pair_t pair_swapped(pair_t p)
{
    pair_t s = malloc(sizeof(struct s_pair_t));
    *s = (struct s_pair_t){p->snd, p->fst};
    return s;
}

pair_t pair_destroy(pair_t p)
{
    free(p);
    p = NULL;
    return p;
}