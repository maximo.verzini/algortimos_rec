#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#include "counter.h"

struct _counter
{
    unsigned int count;
};

counter counter_init(void)
{
    counter c = malloc(sizeof(struct _counter));
    c->count = 0;
    assert(counter_is_init(c));
    return c;
}

void counter_inc(counter c)
{
    assert(c != NULL);
    c->count++;
}

bool counter_is_init(counter c)
{
    return c != NULL && c->count == 0;
}

void counter_dec(counter c)
{
    assert(c != NULL);
    c->count--;
}

counter counter_copy(counter c)
{
    assert(c != NULL);
    counter copy = counter_init();
    copy->count = c->count;
    return copy;
}

void counter_destroy(counter c)
{
    assert(c != NULL);
    c->count = 0;
    free(c);
    c = NULL;
    assert(c == NULL);
}
