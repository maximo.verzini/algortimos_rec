#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *string_clone(const char *str, size_t length)
{
    /// This code won't return anything since it's modifying local
    /// variables that will go out of scope after the function ends.
    /// The given pointer will end up producing undefined behabiours.
    //
    // char clone[length + 1];              // <- Local variable.
    // char *output = clone;                // <- Local scope pointer variable
    //
    // for (size_t i = 0; i < length; i++)
    // {
    //     clone[i] = str[i];
    // }
    //
    // clone[length] = '\0'; // <- Till this point, everything DID copy
    //                       //    correctly on the local scope.
    // return output; <- This will produce undefined behaviour.
    //

    char *clone = malloc(length + 1);
    if (clone == NULL)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < length; i++)
        clone[i] = str[i];
    clone[length] = '\0';

    return clone;
}

int main(void)
{
    char *original = ""
                     "______ time ago in a galaxy far, far away...\n\n\n"
                     "         _______..___________.     ___      .______             \n"
                     "        /       ||           |    /   \\     |   _  \\          \n"
                     "       |   (----``---|  |----`   /  ^  \\    |  |_)  |          \n"
                     "        \\   \\        |  |       /  /_\\  \\   |      /        \n"
                     "    .----)   |       |  |      /  _____  \\  |  |\\  \\----.    \n"
                     "    |_______/        |__|     /__/     \\__\\ | _| `._____|     \n"
                     "                                                                \n"
                     "____    __    ____      ___      .______           _______.     \n"
                     "\\   \\  /  \\  /   /     /   \\     |   _  \\         /       |\n"
                     " \\   \\/    \\/   /     /  ^  \\    |  |_)  |       |   (----` \n"
                     "  \\            /     /  /_\\  \\   |      /         \\   \\    \n"
                     "   \\    /\\    /     /  _____  \\  |  |\\  \\----..----)   |   \n"
                     "    \\__/  \\__/     /__/     \\__\\ | _| `._____||_______/     \n"
                     "\n\n\n"
                     "                     Episode IV \n\n"
                     "                     A NEW HOPE \n\n"
                     "                     It is a period of civil war. Rebel\n"
                     "spaceships, striking from a hidden base, have won their\n"
                     "first victory against the evil Galactic Empire. During the\n"
                     "battle, Rebel spies managed to steal secret plans to the\n"
                     "Empire’s ultimate weapon, the DEATH STAR, an armored space\n"
                     "station with enough power to destroy an entire planet.\n"
                     "Pursued by the Empire’s sinister agents, Princess Leia\n"
                     "races home aboard her starship, custodian of the stolen\n"
                     "plans that can save her people and restore freedom to the\n"
                     "galaxy...\n";
    char *copy = NULL;

    /*-NOTE:--------------------------------------------------------
       I had to replace sizeof(original) / sizeof(char)
       because its results are undesired and undefined.
       gcc: sizeof-pointer-div error
    --------------------------------------------------------------*/
    copy = string_clone(original, strlen(original));
    printf("Original: %s\n", original);
    copy[0] = 'A';
    copy[1] = ' ';
    copy[2] = 'l';
    copy[3] = 'o';
    copy[4] = 'n';
    copy[5] = 'g';
    printf("Copia   : %s\n", copy);

    free(copy);

    return EXIT_SUCCESS;
}
