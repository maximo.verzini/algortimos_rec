#ifndef STRFUNCS_H
#define STRFUNCS_H

#include <stdlib.h>
#define SIZE_MAX 2 << 15

size_t string_length(const char *str);
char *string_filter(const char *src, char c);

#endif