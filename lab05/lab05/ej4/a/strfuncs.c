#include <stdio.h>
#include <assert.h>
#include "strfuncs.h"

const size_t size_max = SIZE_MAX;

size_t string_length(const char *str)
{
    assert(str != NULL);

    size_t c = 0u;

    while (str[c] != '\0' && c < size_max - 1)
    {
        c++;
    }

    if (c == size_max - 1 && str[c] != '\0')
    {
        fprintf(stderr, "String length overflow, is it a valid string?\n");
        exit(EXIT_FAILURE);
    }

    return c;
}

char *string_filter(const char *src, char c)
{
    assert(src != NULL);
    size_t len = string_length(src);

    char *dest = malloc(len);
    if (dest == NULL)
    {
        fprintf(stderr, "Memory allocation error\n");
        exit(EXIT_FAILURE);
    }

    size_t filtered_size = 0u;
    size_t i = 0u;
    while (i < len)
    {
        if (src[i] != c)
        {
            dest[filtered_size] = src[i];
            filtered_size++;
        }
        i++;
    }

    dest = realloc(dest, filtered_size);
    if (dest == NULL)
    {
        fprintf(stderr, "Memory allocation error\n");
        exit(EXIT_FAILURE);
    }

    return dest;
}
