#include <stdlib.h>
#include <assert.h>
#include "stack.h"

struct _s_stack
{
    size_t size;
    struct _s_stack_node *stack;
};

typedef struct _s_stack_node
{
    stack_elem elem;
    struct _s_stack_node *next;
} stack_node;

stack stack_empty()
{
    stack new_stack = malloc(sizeof(struct _s_stack));
    new_stack->size = 0;
    new_stack->stack = NULL;
    return new_stack;
}

stack stack_push(stack s, stack_elem e)
{
    stack_node *new_node = malloc(sizeof(stack_node));
    new_node->elem = e;
    new_node->next = s->stack;

    s->size++;
    s->stack = new_node;

    return s;
}

stack stack_pop(stack s)
{
    assert(!stack_is_empty(s));
    stack_node *head = s->stack;
    s->stack = s->stack->next;
    free(head);
    return s;
}

unsigned int stack_size(stack s)
{
    return s->size;
}

stack_elem stack_top(stack s)
{
    assert(!stack_is_empty(s));
    return s->stack->elem;
}

bool stack_is_empty(stack s)
{
    assert(s != NULL);
    return s->size == 0;
}

stack_elem *stack_to_array(stack s)
{
    unsigned int size = stack_size(s);

    if (size == 0)
        return NULL;

    stack_elem *array = calloc(sizeof(stack_elem), size);
    stack_node *p = s->stack;

    while (p != NULL && size > 0)
    {
        array[size - 1] = p->elem;
        p = p->next;
        size--;
    }

    return array;
}

stack stack_destroy(stack s)
{
    while (!stack_is_empty(s))
    {
        s = stack_pop(s);
    }
    return s;
}