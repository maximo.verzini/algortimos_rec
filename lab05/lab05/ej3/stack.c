#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "stack.h"

struct _s_stack
{
    stack_elem *elems;     // Arreglo de elementos
    unsigned int size;     // Cantidad de elementos en la pila
    unsigned int capacity; // Capacidad actual del arreglo elems
};

stack stack_empty(void)
{
    stack s = malloc(sizeof(struct _s_stack));
    s->size = 0;
    s->capacity = 0;
    s->elems = NULL;
    return s;
}

stack stack_push(stack s, stack_elem e)
{
    s->size++;

    if (s->size > s->capacity)
    {
        s->capacity *= 2;
        s->elems = realloc(s->elems, sizeof(stack_elem) * s->capacity);
    }

    if (s->elems == NULL)
    {
        fprintf(stderr, "Error at reallocating memory.\n");
        exit(EXIT_FAILURE);
    }

    s->elems[s->size - 1] = e;

    return s;
}

stack stack_pop(stack s)
{
    assert(s->size > 0);
    s->size--;
    return s;
}

unsigned int stack_size(stack s)
{
    return s->size;
}

stack_elem stack_top(stack s)
{
    return s->elems[s->size - 1];
}

bool stack_is_empty(stack s)
{
    return s == NULL;
}

stack_elem *stack_to_array(stack s)
{
    stack_elem *array = calloc(s->size, sizeof(stack_elem));

    if (array == NULL && s->size > 0)
    {
        fprintf(stderr, "Error at reallocating memory.\n");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < s->size; i++)
        array[i] = s->elems[i];

    return array;
}

stack stack_destroy(stack s)
{
    assert(s != NULL);
    s->size = 0;
    s->capacity = 0;
    free(s->elems);
    s->elems = NULL;
    free(s);
    s = NULL;

    return s;
}