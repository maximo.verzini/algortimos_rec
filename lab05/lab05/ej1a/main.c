#include <stdio.h>
#include "test.h"

int main(/*int argc, char const *argv[]*/)
{
    stack stack;
    stack_elem array[] = {1, 2, 3, 4, 5};

    stack = stack_empty();
    stack = stack_push(stack, 2);
    bool b_stack_did_push_after_pop_when_stack_size_eq_1 = stack_did_push_after_pop_when_stack_size_eq_1(stack, 1);
    stack_destroy(stack);

    stack = stack_empty();
    bool b_stack_did_push_and_matches_length = stack_did_push_and_matches_length(stack, array, 5);
    bool b_stack_did_copy_array_in_order = stack_did_copy_array_in_order(stack, array, 5);
    stack_destroy(stack);

    stack = stack_empty();
    bool b_stack_did_stack_to_array_when_stack_is_empty = stack_did_stack_to_array_when_stack_is_empty(stack);
    stack_destroy(stack);

    fprintf(stdout, "\n");

    fprintf(stdout, "🧪 stack_did_push_after_pop_when_stack_size_eq_1: %s\n", b_stack_did_push_after_pop_when_stack_size_eq_1 ? "⭕ passed" : "❌ failed");
    fprintf(stdout, "🧪 stack_did_push_and_matches_length: %s\n", b_stack_did_push_and_matches_length ? "⭕ passed" : "❌ failed");
    fprintf(stdout, "🧪 stack_did_copy_array_in_order: %s\n", b_stack_did_copy_array_in_order ? "⭕ passed" : "❌ failed");
    fprintf(stdout, "🧪 stack_did_stack_to_array_when_stack_is_empty: %s\n", b_stack_did_stack_to_array_when_stack_is_empty ? "⭕ passed" : "❌ failed");

    return b_stack_did_push_after_pop_when_stack_size_eq_1 && b_stack_did_push_and_matches_length && b_stack_did_copy_array_in_order && b_stack_did_stack_to_array_when_stack_is_empty;
}
