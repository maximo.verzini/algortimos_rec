
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "stack.h"
#include "test.h"

static void array_dump(stack_elem *elems, unsigned int size)
{
    fprintf(stdout, "[ ");
    if (size > 0)
    {
        fprintf(stdout, "%u", elems[0]);
        for (size_t i = 1; i < size; i++)
        {
            fprintf(stdout, ", %u", elems[i]);
        }
    }
    fprintf(stdout, " ]\n");
}

bool stack_did_push_after_pop_when_stack_size_eq_0(stack s, stack_elem elem)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did push after pop when stack size is 0:\n");
    assert(stack_is_empty(s));
    s = stack_pop(s);
    assert(stack_is_empty(s));
    s = stack_push(s, elem);
    return stack_top(s) == elem;
}

bool stack_did_push_after_pop_when_stack_size_eq_1(stack s, stack_elem elem)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did push after pop when stack size is 1:\n");
    assert(stack_size(s) == 1);

    fprintf(stdout, "Stack size: %u\n", stack_size(s));
    array_dump(stack_to_array(s), stack_size(s));
    fprintf(stdout, "Popping stack...\n");
    s = stack_pop(s);

    fprintf(stdout, "Stack size: %u\n", stack_size(s));
    array_dump(stack_to_array(s), stack_size(s));
    fprintf(stdout, "Pushing %u into stack...\n", elem);
    s = stack_push(s, elem);

    fprintf(stdout, "Stack size: %u\n", stack_size(s));
    array_dump(stack_to_array(s), stack_size(s));

    bool result = stack_top(s) == elem;

    if (result)
        fprintf(stdout, "⭕ test passed.\n");
    else
        fprintf(stderr, "❌ test failed.\n");

    return stack_top(s) == elem;
}

bool stack_did_push(stack s, stack_elem elem)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did push:\n");
    unsigned int prev_size = stack_size(s);

    bool result = prev_size + 1 == stack_size(s = stack_push(s, elem));
    result = result && stack_top(s) == elem;

    fprintf(stdout, "Stack previous size: %u\n", prev_size);
    fprintf(stdout, "Stack current size: %u\n", stack_size(s));
    fprintf(stdout, "element: %u\n", elem);
    fprintf(stdout, "Stack top: %u\n", stack_top(s));

    if (result)
        fprintf(stdout, "⭕ test passed.\n");
    else
        fprintf(stderr, "❌ test failed.\n");

    return result;
}

bool stack_did_pop_when_stack_was_not_empty(stack s)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did pop when stack was not empty:\n");
    unsigned int size = stack_size(s);
    assert(size > 0);
    return stack_size(stack_pop(s)) == size - 1;
}

bool stack_did_pop_when_stack_was_empty(stack s)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did pop when stack was empty:\n");
    unsigned int size = stack_size(s);
    return stack_size(stack_pop(s)) == size;
}

bool stack_did_stack_to_array_when_stack_is_empty(stack s)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did stack to array when stack is empty:\n");
    assert(stack_is_empty(s));

    stack_elem *array = stack_to_array(s);

    array_dump(array, 0);

    if (array == NULL)
    {
        fprintf(stdout, "Array is NULL.\n");
        fprintf(stdout, "⭕ test passed.\n");
        return true;
    }

    fprintf(stderr, "Array is not NULL.\n");
    fprintf(stderr, "❌ test failed.\n");

    return false;
}

bool stack_did_push_and_matches_length(stack s, stack_elem *elems, unsigned int size)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did push and matches length:\n");
    unsigned int prev_size = stack_size(s);

    for (unsigned int i = 0; i < size; i++)
        s = stack_push(s, elems[i]);

    bool result = size + prev_size == stack_size(s);

    fprintf(stdout, "Stack previous size: %u\n", prev_size);
    fprintf(stdout, "Array size: %u\n", size);
    fprintf(stdout, "Expected size: %u\n", size + prev_size);
    fprintf(stdout, "Stack size: %u\n", stack_size(s));

    if (result)
    {
        fprintf(stdout, "Stack size matches array size plus the initial size.\n");
        fprintf(stdout, "⭕ test passed.\n");
    }
    else
    {
        fprintf(stderr, "Stack size does not match array size plus the initial size. Expected  %u, got %u\n", size + prev_size, stack_size(s));
        fprintf(stderr, "❌ test failed.\n");
    }

    return result;
}

bool stack_did_copy_array_in_order(stack s, stack_elem *array, unsigned int length)
{
    fprintf(stdout, "\n\n🧪 (test) Stack did copy array in order:\n");
    bool result = true;
    unsigned int prev_size = stack_size(s);
    stack_elem *stack_dump = stack_to_array(s);

    fprintf(stdout, "Stack dump: ");
    array_dump(stack_dump, prev_size);

    fprintf(stdout, "Array: ");
    array_dump(array, length);

    fprintf(stdout, "Pushing elements to stack...\n");
    fprintf(stdout, "Pushed ");

    for (unsigned int i = 0; i < length; i++)
    {
        fprintf(stdout, "%u ", array[i]);
        s = stack_push(s, array[i]);
    }

    fprintf(stdout, "\n");

    free(stack_dump);
    stack_dump = stack_to_array(s);

    fprintf(stdout, "Stack dump: ");
    array_dump(stack_dump, stack_size(s));

    for (unsigned int i = 0; i < length; i++)
        result = result && stack_dump[prev_size + i] == array[i];

    if (result)
        fprintf(stdout, "⭕ test passed.\n");
    else
        fprintf(stderr, "❌ test failed.\n");

    return result;
}
