
#ifndef TEST_H
#define TEST_H

#include <stdbool.h>
#include "stack.h"

/// @brief Prints the given message with a spinner.
/// @param message
void print_spinner_message(char *message);

/// @brief Wheter the stack did push after pop when the stack size was 0.
/// @param s stack
/// @param elem stack_elem
/// @return
bool stack_did_push_after_pop_when_stack_size_eq_0(stack s, stack_elem elem);

/// @brief Wheter the stack did push after pop when the stack size was 1.
/// @param s stack
/// @param elem stack_elem
/// @return
bool stack_did_push_after_pop_when_stack_size_eq_1(stack s, stack_elem elem);

/// @brief Wheter the stack_push did push the given stack element.
/// @param s stack
/// @param elem stack_elem
/// @return
bool stack_did_push(stack s, stack_elem elem);

/// @brief Wheter the stack did pop.
/// @param s : stack
/// @return
bool stack_did_pop_when_stack_was_not_empty(stack s);

/// @brief Wheter the stack did pop when it was empty.
/// @param s : stack
/// @return
bool stack_did_pop_when_stack_was_empty(stack s);

/// @brief Wheter the stack did stack to array when it was empty.
/// @param s
/// @return
bool stack_did_stack_to_array_when_stack_is_empty(stack s);

/// @brief Pushes into the stack the elements of the array.
/// @param s stack
/// @param elems stack_elem*
/// @param size unsigned int
/// @return Whether the array did push the elements and matches the expected length.
bool stack_did_push_and_matches_length(stack s, stack_elem *elems, unsigned int size);

/// @brief Checks if the stack did output the given array in order.
/// @param s : stack
/// @param array : stack_elem*
/// @param length : unsigned int
/// @return
bool stack_did_copy_array_in_order(stack s, stack_elem *array, unsigned int length);

#endif // TEST_H