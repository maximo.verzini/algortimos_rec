/* First, the standard lib includes, alphabetically ordered */
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Maximum allowed length of the array */
#define MAX_SIZE 100000

unsigned int array_from_stdin(int array[],
                              unsigned int max_size)
{
    int read_val = 0;

    unsigned int size = 0u;

    printf("Input a size for the array: ");
    fscanf(stdin, "%ud", &size);

    unsigned int counter = 0u;
    while (counter < max_size && counter < size)
    {
        printf("Input the %d position of the array: ", counter);
        fscanf(stdin, "%d", &read_val);
        array[counter] = read_val;
        counter++;
    }

    return size;
}

void array_dump(int a[], unsigned int length)
{
    fprintf(stdout, "[");
    for (unsigned int i = 0u; i < length - 1; i++)
    {
        fprintf(stdout, " %d,", a[i]);
    }
    fprintf(stdout, " %d", a[length - 1]);
    fprintf(stdout, "]\n");
}

int main(int argc, char *argv[])
{

    /* create an array of MAX_SIZE elements */
    int array[MAX_SIZE];

    /* parse the file to fill the array and obtain the actual length */
    unsigned int length = array_from_stdin(array, MAX_SIZE);

    /*dumping the array*/
    array_dump(array, length);

    return EXIT_SUCCESS;
}
