#include <stdio.h>
#include "array_helpers.h"

unsigned int array_from_file(int array[],
                             unsigned int max_size,
                             const char *filepath)
{
    FILE *file = fopen(filepath, "r");

    int read_val = 0;

    unsigned int size = 0u;
    fscanf(file, "%d", &size);

    unsigned int counter = 0u;
    while (fscanf(file, "%d", &read_val) != EOF && counter < max_size && counter < size)
    {
        array[counter] = read_val;
        counter++;
    }

    fclose(file);

    return size;
}

void array_dump(int a[], unsigned int length)
{
    fprintf(stdout, "[");
    for (unsigned int i = 0u; i < length - 1; i++)
    {
        fprintf(stdout, " %d,", a[i]);
    }
    fprintf(stdout, " %d", a[length - 1]);
    fprintf(stdout, "]\n");
}