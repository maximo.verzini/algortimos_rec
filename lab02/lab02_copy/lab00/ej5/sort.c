#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort_helpers.h"
#include "sort.h"
#include "fixstring.h"

static unsigned int partition(fixstring a[], unsigned int izq, unsigned int der)
{
    unsigned int pivot = izq;
    unsigned int delta_izq = izq + 1;
    unsigned int delta_der = der;

    while (delta_izq <= delta_der)
    {
        if (goes_before(a[pivot], a[delta_der]))
            delta_der--;
        else if (goes_before(a[delta_izq], a[pivot]))
            delta_izq++;
        else
        {
            swap(a, delta_izq, delta_der);
            delta_der--;
            delta_izq++;
        }
    }

    swap(a, delta_der, pivot);
    return pivot = delta_der;
}

static void quick_sort_rec(fixstring a[], unsigned int izq, unsigned int der)
{
    if (izq >= der)
        return;

    unsigned int pivot = partition(a, izq, der);
    unsigned int left_bound = pivot < der ? pivot + 1 : der;
    unsigned int right_bound = pivot > izq ? pivot - 1 : izq;

    quick_sort_rec(a, left_bound, der);
    quick_sort_rec(a, izq, right_bound);
}

void quick_sort(fixstring a[], unsigned int length)
{
    quick_sort_rec(a, 0, (length == 0) ? 0 : length - 1);
}
