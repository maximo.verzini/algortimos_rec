#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort_helpers.h"
#include "sort.h"

/// @brief Recursively sorts the given array using the quick sort algorithm.
/// @param a Array to sort.
/// @param izq Left bound from where the array will be sorted.
/// @param der Right bound till where the array will be sorted.
static void quick_sort_rec(int a[], unsigned int izq, unsigned int der)
{
    /* needs implementation */

    if (izq >= der)
        return;

    unsigned int pivot = partition(a, izq, der);
    unsigned int left_bound = pivot < der ? pivot + 1 : der;
    unsigned int right_bound = pivot > izq ? pivot - 1 : izq;

    quick_sort_rec(a, left_bound, der);
    quick_sort_rec(a, izq, right_bound);
}

/// @brief Sorts the given array using the quick sort algorithm.
/// @param a Array to sort.
/// @param length Lenght of the given array.
void quick_sort(int a[], unsigned int length)
{
    quick_sort_rec(a, 0u, (length == 0u) ? 0u : length - 1u);
    assert(array_is_sorted(a, length));
}
