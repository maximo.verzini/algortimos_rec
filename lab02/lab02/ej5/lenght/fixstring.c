#include <stdbool.h>
#include <assert.h>

#include "fixstring.h"

unsigned int fstring_length(fixstring s)
{
    unsigned int counter = 0u;
    while (counter < FIXSTRING_MAX && s[counter] != '\n')
        counter++;

    return counter;
}

bool fstring_eq(fixstring s1, fixstring s2)
{
    unsigned int s1_lenght = fstring_length(s1);
    unsigned int s2_lenght = fstring_length(s2);

    if (s1_lenght != s2_lenght)
        return false;

    unsigned int counter = 0u;
    while (
        counter < s1_lenght &&
        s1[counter] == s2[counter])

        counter++;

    return s1[counter] == s2[counter];
}

bool fstring_less_eq(fixstring s1, fixstring s2)
{
    unsigned int s1_lenght = fstring_length(s1);
    unsigned int s2_lenght = fstring_length(s2);

    unsigned int counter = 0u;
    while (
        counter < s1_lenght &&
        counter < s2_lenght &&
        s1[counter] == s2[counter])

        counter++;

    if (s1[counter] <= s2[counter])
        return true;

    return false;
}

void fstring_set(fixstring s1, const fixstring s2)
{
    int i = 0;
    while (i < FIXSTRING_MAX && s2[i] != '\0')
    {
        s1[i] = s2[i];
        i++;
    }
    s1[i] = '\0';
}

void fstring_swap(fixstring s1, fixstring s2)
{
    fixstring aux;
    fstring_set(aux, s1);
    fstring_set(s1, s2);
    fstring_set(s2, aux);
}
