#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort_helpers.h"
#include "sort.h"

static void insert(int a[], size_t i)
{
    size_t counter = i;

    while (counter > 0 && goes_before(a[counter], a[counter - 1]))
    {
        swap(a, counter - 1, counter);
        counter--;
    }
}

void insertion_sort(int a[], size_t length)
{
    for (size_t i = 1u; i < length; ++i)
    {
        insert(a, i);
        assert(array_is_sorted(a, i));
    }
}
