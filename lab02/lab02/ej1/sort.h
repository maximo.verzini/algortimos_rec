#ifndef _SORT_H
#define _SORT_H

#include <stdbool.h>

void insertion_sort(int a[], size_t length);
/*
    Sort the array 'a' using the Insertion sort algorithm. The resulting sort
    will be ascending according to the goes_before funtion.

    The array 'a' must have exactly 'length' elements.

*/

#endif
