#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "array_helpers.h"
#include "sort_helpers.h"
#include "sort.h"

static unsigned int min_pos_from(int a[], unsigned int i, unsigned int length)
{
    unsigned int min_pos = i;
    for (unsigned int j = i + 1; j < length; ++j)
    {
        if (!goes_before(a[min_pos], a[j]))
        {
            min_pos = j;
        }
    }
    return (min_pos);
}

void selection_sort(int a[], unsigned int length)
{
    for (unsigned int i = 0u; i < length; ++i)
    {
        unsigned int min_pos = min_pos_from(a, i, length);
        swap(a, i, min_pos);
    }
}

static void insert(int a[], unsigned int i)
{
    size_t counter = i;

    while (counter > 0 && goes_before(a[counter], a[counter - 1]))
    {
        swap(a, counter - 1, counter);
        counter--;
    }
}

void insertion_sort(int a[], unsigned int length)
{
    for (size_t i = 1u; i < length; ++i)
    {
        insert(a, i);
    }

   
}

/// @brief Function that orders the array from
/// @param a
/// @param izq
/// @param der
/// @return
static unsigned int partition(int a[], unsigned int izq, unsigned int der)
{
    unsigned int pivot = izq;
    unsigned int delta_izq = izq + 1;
    unsigned int delta_der = der;

    while (delta_izq <= delta_der)
    {
        if (goes_before(a[pivot], a[delta_der]))
            delta_der--;
        else if (goes_before(a[delta_izq], a[pivot]))
            delta_izq++;
        else
        {
            swap(a, delta_izq, delta_der);
            delta_der--;
            delta_izq++;
        }
    }

    swap(a, delta_der, pivot);
    return pivot = delta_der;
}

/// @brief Recursively sorts the given array using the quick sort algorithm.
/// @param a Array to sort.
/// @param izq Left bound from where the array will be sorted.
/// @param der Right bound till where the array will be sorted.
static void quick_sort_rec(int a[], unsigned int izq, unsigned int der)
{
    /* needs implementation */

    if (izq >= der)
        return;

    unsigned int pivot = partition(a, izq, der);
    unsigned int left_bound = pivot < der ? pivot + 1 : der;
    unsigned int right_bound = pivot > izq ? pivot - 1 : izq;

    quick_sort_rec(a, left_bound, der);
    quick_sort_rec(a, izq, right_bound);
}

/// @brief Sorts the given array using the quick sort algorithm.
/// @param a Array to sort.
/// @param length Lenght of the given array.
void quick_sort(int a[], unsigned int length)
{
    quick_sort_rec(a, 0u, (length == 0u) ? 0u : length - 1u);
   
}
