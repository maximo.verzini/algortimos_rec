#ifndef _FIXSTRING_H_
#define _FIXSTRING_H_

#include <stdbool.h>

#define FIXSTRING_MAX 100

typedef char fixstring[FIXSTRING_MAX];

/// @brief Returns the length of the string <s>.
/// @param s Array that will be measured.
/// @return unsigned int lenght of <s>.
unsigned int fstring_length(fixstring s);
/*
 * Returns the length of the string <s>
 *
 */

/// @brief Indicates if <s1> has the same content than string <s2>.
/// @param s1 String to compare.
/// @param s2 String to compare.
/// @return boolean representing <s1> == <s2>.
bool fstring_eq(fixstring s1, fixstring s2);
/*
 * Indicates if <s1> has the same content than string <s2>
 *
 */

/// @brief Indicates if string <s1> is less or equal than string <s2> using alphabetical order.
/// @param s1 String to be compared.
/// @param s2 String to compare.
/// @return boolean representing <s1> <= <s2>.
bool fstring_less_eq(fixstring s1, fixstring s2);
/*
 * Indicates if string <s1> is less than string <s2> using alphabetical order
 *
 */

#endif
